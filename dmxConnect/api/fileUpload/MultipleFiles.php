<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_POST": [
      {
        "type": "file",
        "multiple": true,
        "options": {
          "rules": {
            "core:required": {},
            "upload:minfiles": {
              "param": 2
            }
          }
        },
        "name": "files",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "array"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.files}}",
          "path": "/files"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "array",
        "output": false
      },
      {
        "name": "FileUpload",
        "module": "FileUploadModule",
        "action": "APIFileUploadAction",
        "options": {
          "multipleFile": "yes",
          "filesMultipleSource": "grid",
          "APIURL": "{{baseurl+'/dmxConnect/api/UploadMultipleFiles.php'}}",
          "userPass": "kkk3bFb9jtWTtQVqvD03YPNvw5XQZ1kp",
          "userName": "api",
          "path": "{{upload.path}}",
          "fileNameForMultiple": "twofiles",
          "filesMultipleGrid": [
            {
              "value": "{{upload[0].path}}"
            },
            {
              "value": "{{upload[1].path}}"
            }
          ]
        },
        "meta": [
          {
            "name": "response",
            "type": "text"
          }
        ],
        "output": true
      }
    ]
  }
}
JSON
);
?>