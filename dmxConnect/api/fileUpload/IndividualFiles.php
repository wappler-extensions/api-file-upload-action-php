<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_POST": [
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "file1",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "file"
      },
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "file2",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "file"
      },
      {
        "type": "text",
        "name": "fname"
      },
      {
        "type": "text",
        "name": "lname"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.file1}}",
          "path": "/files"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file"
      },
      {
        "name": "upload2",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.file2}}",
          "path": "/files"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file"
      },
      {
        "name": "FileUpload",
        "module": "FileUploadModule",
        "action": "APIFileUploadAction",
        "options": {
          "multipleFile": "no",
          "filesMultipleSource": "array",
          "APIURL": "{{baseurl+'/dmxConnect/api/UploadIndvidualFiles.php'}}",
          "userPass": "kkk3bFb9jtWTtQVqvD03YPNvw5XQZ1kp",
          "userName": "api",
          "path": "{{upload.path}}",
          "filesIndividual": {
            "file1": "{{upload.path}}",
            "file2": "{{upload2.path}}"
          },
          "inputData": {
            "fname": "{{$_POST.fname}}",
            "lname": "{{$_POST.lname}}"
          }
        },
        "meta": [
          {
            "name": "response",
            "type": "text"
          }
        ],
        "output": true
      }
    ]
  }
}
JSON
);
?>