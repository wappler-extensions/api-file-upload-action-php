<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_POST": [
      {
        "type": "file",
        "options": {
          "rules": {
            "upload:maxfiles": {
              "param": 2
            },
            "upload:minfiles": {
              "param": 2
            },
            "core:required": {}
          }
        },
        "name": "files",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "file"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.files}}",
          "path": "/files"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file",
        "output": false
      },
      {
        "name": "FileUpload",
        "module": "FileUploadModule",
        "action": "APIFileUploadAction",
        "options": {
          "multipleFile": "no",
          "filesMultipleSource": "array",
          "APIURL": "{{baseurl+'/dmxConnect/api/UploadIndvidualFiles.php'}}",
          "userPass": "kkk3bFb9jtWTtQVqvD03YPNvw5XQZ1kp",
          "userName": "api",
          "path": "{{upload.path}}",
          "filesIndividual": {
            "file1": "{{upload[0].path}}",
            "file2": "{{upload[1].path}}"
          }
        },
        "meta": [
          {
            "name": "response",
            "type": "text"
          }
        ],
        "output": true
      }
    ]
  }
}
JSON
);
?>