<?php
require('../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_GET": [
      {
        "type": "text",
        "name": "getparam"
      }
    ],
    "$_POST": [
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "manyfiles",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "file"
      },
      {
        "type": "text",
        "name": "postparam"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload1",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.manyfiles}}",
          "path": "/filesDestination"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file",
        "output": true
      },
      {
        "name": "inputData",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{'From POST: '+$_POST.postparam+', From GET: '+$_GET.getparam}}"
        },
        "output": true
      },
      {
        "name": "SA",
        "module": "core",
        "action": "setvalue",
        "options": {
          "key": "SA",
          "value": "UploadMultipleFIlesArray"
        },
        "output": true
      }
    ]
  }
}
JSON
);
?>