<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_POST": [
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "file1",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "file"
      },
      {
        "type": "text",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "Username"
      },
      {
        "type": "text",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "Password"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.file1}}",
          "path": "/files"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file"
      },
      {
        "name": "FileUpload",
        "module": "FileUploadModule",
        "action": "APIFileUploadAction",
        "options": {
          "multipleFile": "no",
          "filesMultipleSource": "array",
          "APIURL": "https://api.tinify.com/shrink",
          "authorization": "basic",
          "userPass": "{{$_POST.Password}}",
          "userName": "{{$_POST.Username}}",
          "uploadMethod": "fileOnly",
          "path": "{{upload.path}}"
        },
        "meta": [
          {
            "name": "response",
            "type": "text"
          }
        ],
        "output": true
      }
    ]
  }
}
JSON
);
?>