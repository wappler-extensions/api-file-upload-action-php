<!doctype html>
<html>

<head>
    <meta name="ac:route" content="/">
    <base href="/">
    <script src="dmxAppConnect/dmxAppConnect.js"></script>
    <meta charset="UTF-8">
    <title>Wappler Extensions - API File Upload Action</title>
    <link rel="icon" href="https://slashash.co/i/logo.svg">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="bootstrap/4/css/bootstrap.min.css" />
    <link rel="stylesheet" href="dmxAppConnect/dmxValidator/dmxValidator.css" />
    <script src="dmxAppConnect/dmxValidator/dmxValidator.js" defer=""></script>
    <script src="dmxAppConnect/dmxBootstrap4Navigation/dmxBootstrap4Navigation.js" defer=""></script>
    <link rel="stylesheet" href="dmxAppConnect/dmxNotifications/dmxNotifications.css" />
    <script src="dmxAppConnect/dmxNotifications/dmxNotifications.js" defer=""></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous" />
</head>

<body is="dmx-app" id="index">
    <dmx-notifications id="notifies1"></dmx-notifications>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <nav class="justify-content-center justify-content-md-between navbar navbar-expand-lg px-0">
                    <a class="d-flex align-items-center navbar-brand text-body" href="https://community.wappler.io/t/about-the-wappler-extensions-category/26436" target="_blank" ref="noopener">
                        <img src="https://community.wappler.io/uploads/default/original/1X/3b802060c30167b96f94ef35fbd0bdf3328d7e3b.png" height="36" class="mr-3">Extensions - API File Upload Action</a>
                    <a class="navbar-brand" href="http://slashash.co" target="_blank" title="Designed &amp; Developed by Slashash Tech LLP" ref="noopener">
                        <img src="https://slashash.co/i/made_by_SLASHASH.svg" alt="Designed &amp; Developed by Slashash Tech LLP"></a>
                </nav>
            </div>
        </div>
    </div>
    <div class="container-fluid pb-5 pt-md-5 pt-2 wappler-block">
        <ul class="nav nav-tabs" id="navTabsTabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="navTabsAbout" data-toggle="tab" href="#" data-target="#tabAbout" role="tab" aria-controls="tabAbout" aria-selected="false">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="navTabsFileOnly" data-toggle="tab" href="#" data-target="#tabFileOnly" role="tab" aria-controls="tabFileOnly" aria-selected="true">File Only (Binary)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="navTabsIndividualFile" data-toggle="tab" href="#" data-target="#tabIndividualFile" role="tab" aria-controls="tabIndividualFile" aria-selected="false">Individual Files</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="navTabsMultiIndFile" data-toggle="tab" href="#" data-target="#tabMultiIndFile" role="tab" aria-controls="tabMultiIndFile" aria-selected="false">Multiple Files Uploaded Individually</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="navTabsMultipleFiles" data-toggle="tab" href="#" data-target="#tabMultipleFiles" role="tab" aria-controls="tabMultipleFiles" aria-selected="false">Multiple Files</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="navTabsMultipleArray" data-toggle="tab" href="#" data-target="#tabMultipleArray" role="tab" aria-controls="tabMultipleArray" aria-selected="false">Multiple Files 2</a>
            </li>
        </ul>
        <div class="tab-content border border-top-0" id="navTabsContent">
            <div class="tab-pane fade active show p-3" id="tabAbout" role="tabpanel">
                <h4><i class="fas fa-lg fa-file-upload mr-2"></i>API File Upload Action</h4>
                <p>Wappler provides a default API Action module in Server Action to call APIs. But it does not have the option to upload files. With this custom module, we are trying to extend Wappler to have this option as well. We have tried to
                    provide as many options we could from the original component here. This module is available for both PHP & NodeJS server models.</p>
                <p>Almost all options are explained using <i class="border py-1 px-2 border-dark fas fa-info"></i> help texts. Please make sure to read them.</p>
                <p>The examples here use images uploaded from the client side. So first, we save the image in server, and then supply the path to our custom module.<br />This allows for API File Upload Action to be used with images already stored,
                    paths from DB, URL paths and also if images are being uploaded from the client side.</p>
                <p><b>NOTE</b>: The base URL for API URL used in examples is set in Globals. Please modify that as per your setup.</p>
                <p>Please feel free to raise issues or post feature requests on the Git repositories.</p>
                <h4 class="mt-4">Known Issues</h4>
                <ul>
                    <li>Input fields and grids might appear incorrectly when switching between various options. You might even see them when opening the step after saving. This is a <a href="https://community.wappler.io/t/custom-module-conditional-items-show-up-incorrectly/29550" target="_blank" rel="noopener">reported</a> Wappler bug. Please be vigilant when
                        configuring the module until this is fixed by Wappler team.</li>
                </ul>
                <h4 class="mt-4">Download</h4>
                <div class="d-block mb-2">Main custom module files are stored in <mark>extensions/server_connect/modules</mark>.</div>
                <span>
                    NodeJS:
                    <a title="Git Lab" class="border rounded p-2 ml-2" rel="noopener" href="https://gitlab.com/wappler-extensions/api-file-upload-action-nodejs" target="_blank">
                        <svg width="24" height="24" class="tanuki-logo" viewBox="0 0 36 36">
                            <path class="tanuki-shape tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"></path>
                            <path class="tanuki-shape tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"></path>
                            <path class="tanuki-shape tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"></path>
                            <path class="tanuki-shape tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"></path>
                            <path class="tanuki-shape tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"></path>
                            <path class="tanuki-shape tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"></path>
                            <path class="tanuki-shape tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"></path>
                        </svg>
                        <svg width="42" height="42" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 617 169">
                            <path
                                d="M315.26 2.97h-21.8l.1 162.5h88.3v-20.1h-66.5l-.1-142.4M465.89 136.95c-5.5 5.7-14.6 11.4-27 11.4-16.6 0-23.3-8.2-23.3-18.9 0-16.1 11.2-23.8 35-23.8 4.5 0 11.7.5 15.4 1.2v30.1h-.1m-22.6-98.5c-17.6 0-33.8 6.2-46.4 16.7l7.7 13.4c8.9-5.2 19.8-10.4 35.5-10.4 17.9 0 25.8 9.2 25.8 24.6v7.9c-3.5-.7-10.7-1.2-15.1-1.2-38.2 0-57.6 13.4-57.6 41.4 0 25.1 15.4 37.7 38.7 37.7 15.7 0 30.8-7.2 36-18.9l4 15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9M557.63 149.1c-8.2 0-15.4-1-20.8-3.5V70.5c7.4-6.2 16.6-10.7 28.3-10.7 21.1 0 29.2 14.9 29.2 39 0 34.2-13.1 50.3-36.7 50.3m9.2-110.6c-19.5 0-30 13.3-30 13.3v-21l-.1-27.8h-21.3l.1 158.5c10.7 4.5 25.3 6.9 41.2 6.9 40.7 0 60.3-26 60.3-70.9-.1-35.5-18.2-59-50.2-59M77.9 20.6c19.3 0 31.8 6.4 39.9 12.9l9.4-16.3C114.5 6 97.3 0 78.9 0 32.5 0 0 28.3 0 85.4c0 59.8 35.1 83.1 75.2 83.1 20.1 0 37.2-4.7 48.4-9.4l-.5-63.9V75.1H63.6v20.1h38l.5 48.5c-5 2.5-13.6 4.5-25.3 4.5-32.2 0-53.8-20.3-53.8-63-.1-43.5 22.2-64.6 54.9-64.6M231.43 2.95h-21.3l.1 27.3v94.3c0 26.3 11.4 43.9 43.9 43.9 4.5 0 8.9-.4 13.1-1.2v-19.1c-3.1.5-6.4.7-9.9.7-17.9 0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5M155.96 165.47h21.3v-124h-21.3v124M155.96 24.37h21.3V3.07h-21.3v21.3">
                            </path>
                        </svg>
                    </a>
                </span>
                <span class="mx-2 font-weight-bold lead">|</span>
                <span>
                    PHP:
                    <a title="Git Lab" class="border rounded p-2 ml-2" rel="noopener" href="https://gitlab.com/wappler-extensions/api-file-upload-action-php" target="_blank">
                        <svg width="24" height="24" class="tanuki-logo" viewBox="0 0 36 36">
                            <path class="tanuki-shape tanuki-left-ear" fill="#e24329" d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"></path>
                            <path class="tanuki-shape tanuki-right-ear" fill="#e24329" d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"></path>
                            <path class="tanuki-shape tanuki-nose" fill="#e24329" d="M18,34.38 3,14 33,14 Z"></path>
                            <path class="tanuki-shape tanuki-left-eye" fill="#fc6d26" d="M18,34.38 11.38,14 2,14 6,25Z"></path>
                            <path class="tanuki-shape tanuki-right-eye" fill="#fc6d26" d="M18,34.38 24.62,14 34,14 30,25Z"></path>
                            <path class="tanuki-shape tanuki-left-cheek" fill="#fca326" d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"></path>
                            <path class="tanuki-shape tanuki-right-cheek" fill="#fca326" d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"></path>
                        </svg>
                        <svg width="42" height="42" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 617 169">
                            <path
                                d="M315.26 2.97h-21.8l.1 162.5h88.3v-20.1h-66.5l-.1-142.4M465.89 136.95c-5.5 5.7-14.6 11.4-27 11.4-16.6 0-23.3-8.2-23.3-18.9 0-16.1 11.2-23.8 35-23.8 4.5 0 11.7.5 15.4 1.2v30.1h-.1m-22.6-98.5c-17.6 0-33.8 6.2-46.4 16.7l7.7 13.4c8.9-5.2 19.8-10.4 35.5-10.4 17.9 0 25.8 9.2 25.8 24.6v7.9c-3.5-.7-10.7-1.2-15.1-1.2-38.2 0-57.6 13.4-57.6 41.4 0 25.1 15.4 37.7 38.7 37.7 15.7 0 30.8-7.2 36-18.9l4 15.9h15.4v-83.2c-.1-26.3-11.5-43.9-44-43.9M557.63 149.1c-8.2 0-15.4-1-20.8-3.5V70.5c7.4-6.2 16.6-10.7 28.3-10.7 21.1 0 29.2 14.9 29.2 39 0 34.2-13.1 50.3-36.7 50.3m9.2-110.6c-19.5 0-30 13.3-30 13.3v-21l-.1-27.8h-21.3l.1 158.5c10.7 4.5 25.3 6.9 41.2 6.9 40.7 0 60.3-26 60.3-70.9-.1-35.5-18.2-59-50.2-59M77.9 20.6c19.3 0 31.8 6.4 39.9 12.9l9.4-16.3C114.5 6 97.3 0 78.9 0 32.5 0 0 28.3 0 85.4c0 59.8 35.1 83.1 75.2 83.1 20.1 0 37.2-4.7 48.4-9.4l-.5-63.9V75.1H63.6v20.1h38l.5 48.5c-5 2.5-13.6 4.5-25.3 4.5-32.2 0-53.8-20.3-53.8-63-.1-43.5 22.2-64.6 54.9-64.6M231.43 2.95h-21.3l.1 27.3v94.3c0 26.3 11.4 43.9 43.9 43.9 4.5 0 8.9-.4 13.1-1.2v-19.1c-3.1.5-6.4.7-9.9.7-17.9 0-25.8-9.2-25.8-24.6v-65h35.7v-17.8h-35.7l-.1-38.5M155.96 165.47h21.3v-124h-21.3v124M155.96 24.37h21.3V3.07h-21.3v21.3">
                            </path>
                        </svg>
                    </a>
                </span>
            </div>
            <div class="tab-pane fade p-3" id="tabFileOnly" role="tabpanel">
                <h4>Sending Single File as Request Body</h4>
                <p>For uploading a single file to an API which requires the file to be the request body itself in binary format.<br />Selected file gets uploaded to the server action where custom module send it to API service as a binary file in the
                    request body.</p>
                <p>We have used <a href="https://tinypng.com/developers/reference" rel="noopener" target="_blank">Tiny PNG</a> as an example API service here. You can signup for their free developer account to test it out yourself.<br />Server
                    Actions:
                    <mark>FileUpload/SingleFileUpload</mark></p>
                <div class="row">
                    <div class="col-md-6 col-12">
                        <form is="dmx-serverconnect-form" id="formSingleFileUpload" method="post" action="dmxConnect/api/FileUpload/SingleFileUpload.php">
                            <input type="hidden" name="Username" value="api">
                            <h6 class="mt-2">REQUEST:</h6>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="fileSingleUpload">Select 1 File</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="file" id="fileSingleUpload" name="file1" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="pass">API Key (Used in HTTP Basic Auth)</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" id="pass" name="Password" required="">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" dmx-bind:disabled="state.executing">Submit<span class="spinner-border spinner-border-sm ml-2" role="status" dmx-show="state.executing"></span>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-6 col-12">
                        <h6 class="mt-4 mt-md-2">RESPONSE:</h6>
                        <pre style="height:200px;" class="bg-light p-2 border rounded-lg" dmx-class:text-danger="formSingleFileUpload.status != 200">{{formSingleFileUpload.status != 200 ? formSingleFileUpload.lastError.response.message : formSingleFileUpload.data.stringifyJSON()}}</pre>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade p-3" id="tabIndividualFile" role="tabpanel">
                <h4>Uploading Multiple Files Separately</h4>
                <p>You can upload one or more files with different key-names. And, you can send other GET or POST data along with the files too.</p>
                <p>Server Actions: <mark>FileUpload/IndividualFiles</mark>, <mark>UploadIndvidualFiles</mark></p>
                <div class="row">
                    <div class="col-md-6 col-12">
                        <form is="dmx-serverconnect-form" id="formIndFileUpload" method="post" action="dmxConnect/api/FileUpload/IndividualFiles.php">
                            <h6 class="mt-2">REQUEST:</h6>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="fileIndividual1">File 1</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="file" class=" " id="fileIndividual1" name="file1" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="fileIndividual2">File 2</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="file" class=" " id="fileIndividual2" name="file2" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="txtFirstName">First Name</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" id="txtFirstName" name="fname">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="txtLastName">Last Name</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" id="txtLastName" name="lname">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" dmx-bind:disabled="state.executing">Submit<span class="spinner-border spinner-border-sm ml-2" role="status" dmx-show="state.executing"></span>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-6 col-12">
                        <h6 class="mt-4 mt-md-2">RESPONSE:</h6>
                        <pre style="height:200px;" class="bg-light p-2 border rounded-lg" dmx-class:text-danger="formIndFileUpload.status != 200">{{formIndFileUpload.status != 200 ? formIndFileUpload.lastError.response.message : formIndFileUpload.data.stringifyJSON()}}</pre>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade p-3" id="tabMultiIndFile" role="tabpanel">
                <h4>Upload Multiple Selected Files Individually</h4>
                <p>You can also configure the module to upload multiple files sent to server action as inividual files, if you have a restriction/control on number of files. This is a very specific rare use case which is possible without any additional changes in the module's code.</p>
                <p>Server Actions: <mark>FileUpload/MultipleFileIndividualUpload</mark>, <mark>UploadIndvidualFiles</mark></p>
                <div class="row">
                    <div class="col-md-6 col-12">
                        <form is="dmx-serverconnect-form" id="formMultiIndFileUpload" method="post" action="dmxConnect/api/FileUpload/MultipleFileIndividualUpload.php">
                            <h6 class="mt-2">REQUEST:</h6>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="fileMultiInd">Select 2 Files</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="file" id="fileMultiInd" name="files[]" multiple data-rule-maxfiles="2" data-msg-maxfiles="Please select no more than 2 files." data-rule-minfiles="2" required="">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" dmx-bind:disabled="state.executing">Submit<span class="spinner-border spinner-border-sm ml-2" role="status" dmx-show="state.executing"></span>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-6 col-12">
                        <h6 class="mt-4 mt-md-2">RESPONSE:</h6>
                        <pre style="height:200px;" class="bg-light p-2 border rounded-lg" dmx-class:text-danger="formMultiIndFileUpload.status != 200">{{formMultiIndFileUpload.status != 200 ? formMultiIndFileUpload.lastError.response.message : formMultiIndFileUpload.data.stringifyJSON()}}</pre>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade p-3" id="tabMultipleFiles" role="tabpanel">
                <h4>Select & Upload Multiple Files</h4>
                <p>The file upload action can also handle multiple file uploads with any number of files. The list of files can be supplied in a grid input.</p>
                <p>In the example server action, only first two files sent from here are uploaded.</p>
                <p>Server Actions: <mark>FileUpload/MultipleFiles</mark>, <mark>UploadMultipleFIles</mark></p>
                <div class="row">
                    <div class="col-md-6 col-12">
                        <form is="dmx-serverconnect-form" id="formMultipleFiles" method="post" action="dmxConnect/api/FileUpload/MultipleFiles.php">
                            <h6 class="mt-2">REQUEST:</h6>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="fileMultiple">Select at least 2 Files</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="file" id="fileMultiple" name="files[]" multiple data-rule-minfiles="2" required="">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" dmx-bind:disabled="state.executing">Submit<span class="spinner-border spinner-border-sm ml-2" role="status" dmx-show="state.executing"></span>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-6 col-12">
                        <h6 class="mt-4 mt-md-2">RESPONSE:</h6>
                        <pre style="height:200px;" class="bg-light p-2 border rounded-lg" dmx-class:text-danger="formMultipleFiles.status != 200">{{formMultipleFiles.status != 200 ? formMultipleFiles.lastError.response.message : formMultipleFiles.data.stringifyJSON()}}</pre>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade p-3" id="tabMultipleArray" role="tabpanel">
                <h4>Upload Multiple Files via Array</h4>
                <p>When the list of files to be upload is dynamic, custom file upload action can accept the list as an array of file paths (or URLs) too. And, you can send other GET or POST data along with the files too.</p>
                <p>In the example server action, all files sent from here will be uploaded.</p>
                <p>Server Actions: <mark>FileUpload/MultipleFilesArray</mark>, <mark>UploadMultipleFIlesArray</mark></p>
                <div class="row">
                    <div class="col-md-6 col-12">
                        <form is="dmx-serverconnect-form" id="formMultipleFilesArray" method="post" action="dmxConnect/api/FileUpload/MultipleFilesArray.php">
                            <h6 class="mt-2">REQUEST:</h6>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="fileMultipleArray">Select Max 10 Files</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="file" id="fileMultipleArray" name="files[]" multiple required="" data-msg-minfiles="" data-rule-maxfiles="10">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="txtPostParam">Post Param</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" id="txtPostParam" name="PostParam">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-5 col-12">
                                    <label for="txtGetParam">Get Param</label>
                                </div>
                                <div class="col-md-6 col-12">
                                    <input type="text" class="form-control" id="txtGetParam" name="GetParam">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" dmx-bind:disabled="state.executing">Submit<span class="spinner-border spinner-border-sm ml-2" role="status" dmx-show="state.executing"></span>
                            </button>
                        </form>
                    </div>
                    <div class="col-md-6 col-12">
                        <h6 class="mt-4 mt-md-2">RESPONSE:</h6>
                        <pre style="height:200px;" class="bg-light p-2 border rounded-lg" dmx-class:text-danger="formMultipleFilesArray.status != 200">{{formMultipleFilesArray.status != 200 ? formMultipleFilesArray.lastError.response.message : formMultipleFilesArray.data.stringifyJSON()}}</pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="bootstrap/4/js/popper.min.js"></script>
    <script src="bootstrap/4/js/bootstrap.min.js"></script>
    <script>
        $(function () {
            dmx.Formatter('object', 'stringifyJSON', function (param) {
                return JSON.stringify(param, null, 2);
            }
            );
        });
    </script>
</body>

</html>