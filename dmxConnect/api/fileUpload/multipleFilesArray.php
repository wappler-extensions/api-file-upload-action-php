<?php
require('../../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_POST": [
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {},
            "upload:maxfiles": {
              "param": 10
            }
          }
        },
        "name": "files",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "file"
      },
      {
        "type": "text",
        "name": "PostParam"
      },
      {
        "type": "text",
        "name": "GetParam"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.files}}",
          "path": "/files"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file"
      },
      {
        "name": "FileUpload",
        "module": "FileUploadModule",
        "action": "APIFileUploadAction",
        "options": {
          "multipleFile": "yes",
          "filesMultipleSource": "array",
          "APIURL": "{{baseurl+'/dmxConnect/api/UploadMultipleFilesArray.php'}}",
          "userPass": "kkk3bFb9jtWTtQVqvD03YPNvw5XQZ1kp",
          "userName": "api",
          "path": "{{upload.path}}",
          "fileNameForMultiple": "manyfiles",
          "filesMultipleGrid": [
            {
              "value": "{{upload.path}}"
            },
            {
              "value": "{{upload2.path}}"
            }
          ],
          "filesMultipleArr": "{{upload}}",
          "filesMultipleArrPath": "path",
          "inputData": {
            "postparam": "{{$_POST.PostParam}}"
          },
          "queryData": {
            "getparam": "{{$_POST.GetParam}}"
          }
        },
        "meta": [
          {
            "name": "response",
            "type": "text"
          }
        ],
        "output": true
      }
    ]
  }
}
JSON
);
?>