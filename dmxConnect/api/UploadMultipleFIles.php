<?php
require('../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_POST": [
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "twofiles",
        "sub": [
          {
            "type": "text",
            "name": "name"
          },
          {
            "type": "text",
            "name": "type"
          },
          {
            "type": "number",
            "name": "size"
          },
          {
            "type": "text",
            "name": "error"
          }
        ],
        "outputType": "file"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload1",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.twofiles}}",
          "path": "/filesDestination"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file",
        "output": true
      },
      {
        "name": "SA",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "UploadMultipleFiles",
          "key": "SA"
        },
        "output": true
      }
    ]
  }
}
JSON
);
?>