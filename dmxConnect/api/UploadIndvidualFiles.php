<?php
require('../../dmxConnectLib/dmxConnect.php');


$app = new \lib\App();

$app->define(<<<'JSON'
{
  "meta": {
    "$_POST": [
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "file1",
        "sub": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "number"
          },
          {
            "name": "error",
            "type": "text"
          }
        ],
        "outputType": "file"
      },
      {
        "type": "file",
        "options": {
          "rules": {
            "core:required": {}
          }
        },
        "name": "file2",
        "sub": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "number"
          },
          {
            "name": "error",
            "type": "text"
          }
        ],
        "outputType": "file"
      },
      {
        "type": "text",
        "name": "fname"
      },
      {
        "type": "text",
        "name": "lname"
      }
    ]
  },
  "exec": {
    "steps": [
      {
        "name": "upload1",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.file1}}",
          "path": "/filesDestination"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file",
        "output": true
      },
      {
        "name": "upload2",
        "module": "upload",
        "action": "upload",
        "options": {
          "fields": "{{$_POST.file2}}",
          "path": "/filesDestination"
        },
        "meta": [
          {
            "name": "name",
            "type": "text"
          },
          {
            "name": "path",
            "type": "text"
          },
          {
            "name": "url",
            "type": "text"
          },
          {
            "name": "type",
            "type": "text"
          },
          {
            "name": "size",
            "type": "text"
          },
          {
            "name": "error",
            "type": "number"
          }
        ],
        "outputType": "file",
        "output": true
      },
      {
        "name": "inputData",
        "module": "core",
        "action": "setvalue",
        "options": {
          "value": "{{'First Name: '+$_POST.fname+', Last Name: '+$_POST.lname}}"
        },
        "output": true
      },
      {
        "name": "SA",
        "module": "core",
        "action": "setvalue",
        "options": {
          "key": "SA",
          "value": "UploadIndvidualFiles"
        },
        "output": true
      }
    ]
  }
}
JSON
);
?>